﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO.Ports;
using System.Runtime.InteropServices;

namespace SerialDemo
{
    public partial class ComSet : Form
    {
        /// <summary>
        /// 设定样式用的常量
        /// </summary>
        private const int SC_CLOSE = 0xF060;
        private const int MF_ENABLED = 0x00000000;
        private const int MF_GRAYED = 0x00000001;
        private const int MF_DISABLED = 0x00000002;

        /// <summary>
        /// 获取指定句柄的窗体的标题栏
        /// </summary>
        [DllImport("user32.dll", EntryPoint = "GetSystemMenu")]
        private static extern IntPtr GetSystemMenu(IntPtr hWnd, int bRevert);

        /// <summary>
        /// 设置标题栏的关闭按钮的样式
        /// </summary>
        [DllImport("User32.dll")]
        public static extern bool EnableMenuItem(IntPtr hMenu, int uIDEnableItem, int uEnable);

        public ComSet()
        {
            InitializeComponent();
            IntPtr hMenu = GetSystemMenu(this.Handle, 0);
            EnableMenuItem(hMenu, SC_CLOSE, (MF_DISABLED + MF_GRAYED) | MF_ENABLED);
        }
        /// <summary>
        /// 禁用窗体移动等操作
        /// </summary>
        /// <param name="m"></param>
        //protected override void WndProc(ref Message m)
        //{
        //    if (m.Msg == 0xa3 || (m.Msg == 0x00A1 && m.WParam.ToInt32() == 2) || m.WParam.ToInt32() == 0xF010)
        //    {
        //        return;
        //    }
        //    base.WndProc(ref m);
        //}

        /// <summary>
        /// 加载
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ComSet_Load(object sender, EventArgs e)
        {
            //串口
            cmbPort.Items.Clear();
            cmbBaudRate.Items.Clear();
            cmbDataBits.Items.Clear();
            cmbStopBit.Items.Clear();
            cmbParity.Items.Clear();
            string[] ports = SerialPort.GetPortNames();
            foreach (string port in ports)
            {
                cmbPort.Items.Add(port);
            }

            cmbPort.SelectedIndex = ports.Length-1;

            //波特率
            cmbBaudRate.Items.Add("9600");
            cmbBaudRate.Items.Add("19200");
            cmbBaudRate.Items.Add("38400");
            cmbBaudRate.Items.Add("115200");
            cmbBaudRate.Items.Add("2000000");
            cmbBaudRate.Items.Add("1000000");
            cmbBaudRate.SelectedIndex = 3;

            //数据位
            cmbDataBits.Items.Add("8");
            cmbDataBits.SelectedIndex = 0;
            //停止位
            cmbStopBit.Items.Add("1");
            cmbStopBit.SelectedIndex = 0;
            //校验位
            cmbParity.Items.Add("无");
            cmbParity.SelectedIndex = 0;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            SerialForm.strPortName = cmbPort.Text;
            SerialForm.strBaudRate = cmbBaudRate.Text;
            SerialForm.strDataBits = cmbDataBits.Text;
            SerialForm.strStopBits = cmbStopBit.Text;
            DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}