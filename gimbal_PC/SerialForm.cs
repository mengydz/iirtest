﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO.Ports;
using System.IO;
using System.Threading;
using System.Runtime.InteropServices;//引用命名空间

using System.Collections;
using ZedGraph;

namespace SerialDemo
{
    public partial class SerialForm : Form
    {
        Logo noframe = new Logo();
        ComSet comset = new ComSet();
        
        public static string strPortName = "";
        public static string strBaudRate = "";
        public static string strDataBits = "";
        public static string strStopBits = "";
        bool flagstartup = false;

        #region "配置文件声明变量"
        /// <summary>
        /// 写入INI文件
        /// </summary>
        /// <param name="section">节点名称[如[TypeName]]</param>
        /// <param name="key">键</param>
        /// <param name="val">值</param>
        /// <param name="filepath">文件路径</param>
        /// <returns></returns>
        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section, string key, string val, string filepath);
        /// <summary>
        /// 读取INI文件
        /// </summary>
        /// <param name="section">节点名称</param>
        /// <param name="key">键</param>
        /// <param name="def">值</param>
        /// <param name="retval">stringbulider对象</param>
        /// <param name="size">字节大小</param>
        /// <param name="filePath">文件路径</param>
        /// <returns></returns>
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retval, int size, string filePath);

        private string strFilePath = Application.StartupPath + "\\FileConfig.ini";//获取INI文件路径

        #endregion

        /// <summary>
        /// 自定义读取INI文件中的内容方法
        /// </summary>
        /// <param name="Section">键</param>
        /// <param name="key">值</param>
        /// <returns></returns>
        private string ContentValue(string Section, string key)
        {

            StringBuilder temp = new StringBuilder(1024);
            GetPrivateProfileString(Section, key, "", temp, 1024, strFilePath);
            return temp.ToString();
        }

        public SerialForm()
        {
            InitializeComponent();
            noframe.ShowDialog();
            comset.ShowDialog();
            Init_Curves();
            //Pitch
            EPitchP.Text = ContentValue("PITCHPID", "EPitchP");
            IPitchP.Text = ContentValue("PITCHPID", "IPitchP");
            IPitchI.Text = ContentValue("PITCHPID", "IPitchI");
            IPitchD.Text = ContentValue("PITCHPID", "IPitchD");
            //Roll
            ERollP.Text = ContentValue("ROLLPID", "ERollP");
            IRollP.Text = ContentValue("ROLLPID", "IRollP");
            IRollI.Text = ContentValue("ROLLPID", "IRollI");
            IRollD.Text = ContentValue("ROLLPID", "IRollD");
            //Yaw
            EYawP.Text = ContentValue("YAWPID", "EYawP");
            IYawP.Text = ContentValue("YAWPID", "IYawP");
            IYawI.Text = ContentValue("YAWPID", "IYawI");
            IYawD.Text = ContentValue("YAWPID", "IYawD");
            flagstartup = true;
        }

        public float StrToFloat(object FloatString)
        {
            float result;
            if (FloatString != null)
            {
                if (float.TryParse(FloatString.ToString(), out result))
                    return result;
                else
                {
                    return (float)0.00;
                }
            }
            else
            {
                return (float)0.00;
            }
        }

        #region "串口接收"
        Int16 nCaseCount = 1;
        Int16 nRevBuffCount = 0;
        bool Rev_Ok_Flag = false;
        byte[] RecieveBuf = new byte[100];

        void Rev_Deal(byte byteRev)
        {
            switch (nCaseCount)
            {
                case 1:
                    {
                        if (byteRev == 0xAA)
                        {
                            RecieveBuf[nRevBuffCount] = byteRev;
                            nCaseCount++;
                            nRevBuffCount++;
                        }
                        else
                        {
                            nCaseCount = 1;
                            nRevBuffCount = 0;
                        }
                    } break;
                case 2:
                    {
                        if (byteRev == 0x55)
                        {
                            RecieveBuf[nRevBuffCount] = byteRev;
                            nCaseCount++;
                            nRevBuffCount++;
                        }
                        else
                        {
                            nCaseCount = 1;
                            nRevBuffCount = 0;
                        }
                    } break;
                case 3:
                    {
                        if (byteRev == 0x02)
                        {
                            RecieveBuf[nRevBuffCount] = byteRev;
                            nCaseCount++;
                            nRevBuffCount++;
                        }
                        else
                        {
                            nCaseCount = 1;
                            nRevBuffCount = 0;
                        }
                    } break;
                default:
                    {
                        RecieveBuf[nRevBuffCount] = byteRev;
                        if (nCaseCount == RecieveBuf[3])
                        {
                            nCaseCount = 1;
                            nRevBuffCount = 0;
                            Rev_Ok_Flag = true;
                        }
                        else
                        {
                            nCaseCount++;
                            nRevBuffCount++;
                        }
                    } break;
            }
        }

        float curve1_show = new float();
        float curve2_show = new float();
        int curve_timstamp_l = 0;
        private void receive_send()
        {
            //这里是用来收发数据
            try
            {
                while (serialPort1.IsOpen)
                {
                    Rev_Deal((byte)serialPort1.ReadByte());

                    if (Rev_Ok_Flag)
                    {
                        Rev_Ok_Flag = false;
                        Invoke(new MethodInvoker(delegate() { txtRecieve.AppendText(byteToHexStr(RecieveBuf)+"\n"); }));
                        //
                        curve1_show = BitConverter.ToSingle(RecieveBuf, 4);
                        list1.Add(curve_timstamp_l, curve1_show);
                        if (list1.Count >= 2000)
                            list1.RemoveAt(0);
                        //
                        curve2_show = BitConverter.ToSingle(RecieveBuf, 8);
                        list2.Add(curve_timstamp_l, curve2_show);
                        if (list2.Count >= 2000)
                            list2.RemoveAt(0);

                        //Invoke(new MethodInvoker(delegate()
                        //{
                        //    zedGraphControl1.AxisChange();
                        //    zedGraphControl2.AxisChange();
                        //    zedGraphControl1.Refresh();
                        //    zedGraphControl2.Refresh();
                        //    zedGraphControl1.Invalidate();
                        //    zedGraphControl2.Invalidate();
                        //}));
                        curve_timstamp_l++;
                   }
                }
            }catch{}

        }
        #endregion

        private void Menu_Ctl_Port_Click(object sender, EventArgs e)
        {
            if (!serialPort1.IsOpen)
            {
                Menu_Ctl_Port.Text = "关闭串口";
                Status_Com.Text = "串口" + strPortName + "已打开";

                serialPort1.PortName = strPortName;
                serialPort1.BaudRate = int.Parse(strBaudRate);
                serialPort1.DataBits = int.Parse(strDataBits);
                serialPort1.StopBits = (StopBits)int.Parse(strStopBits);
                //打开
                serialPort1.Open();
                Thread CollectThread = new Thread(receive_send);
                CollectThread.Start();
            }
            else
            {
                Menu_Ctl_Port.Text = "打开串口";
                Status_Com.Text = "串口关闭";
                serialPort1.Close();
            }
        }

        //清空
        private void btnClear_Click(object sender, EventArgs e)
        {
            if ((sender as Button) ==btnClearRead)
            {
                txtRecieve.Clear();
            }
        }

        private void SerialForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            serialPort1.Close();
        }

        private void Menu_Set_Comset_Click(object sender, EventArgs e)
        {
            comset.ShowDialog();
        }

        private void Menu_About_Click(object sender, EventArgs e)
        {
            About about = new About();
            about.Show();
        }

        private string byteToHexStr(byte[] bytes)
        {
            string returnStr = "";
            if (bytes != null)
            {
                for (int i = 0; i < bytes[3]+4; i++)
                {
                    returnStr += bytes[i].ToString("X2")+" ";
                }
            }
            return returnStr;
        }

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////绘图区域////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        #region "声明变量"
        PointPairList list1 = new PointPairList();
        Color Line_color1 = new Color();
        string Line_name1 = "";
        GraphPane myPane1;
        LineItem myCurve1;

        PointPairList list2 = new PointPairList();
        Color Line_color2 = new Color();
        string Line_name2 = "";
        GraphPane myPane2;
        LineItem myCurve2;

        private void Init_Curves()
        {
            list1 = new PointPairList();      //必须实例化
            Line_color1 = Color.Red;
            //Line_name1 = "Pitch";

            list2 = new PointPairList();      //必须实例化
            Line_color2 = Color.Red;
            //Line_name2 = "Roll";

            creatGraph_online();
        }

        private void creatGraph_online()
        {
            zedGraphControl1.Name = "zedGraphControl1";
            zedGraphControl1.ScrollGrace = 0D;
            zedGraphControl1.ScrollMaxX = 0D;
            zedGraphControl1.ScrollMaxY = 0D;
            zedGraphControl1.ScrollMaxY2 = 0D;
            zedGraphControl1.ScrollMinX = 0D;
            zedGraphControl1.ScrollMinY = 0D;
            zedGraphControl1.ScrollMinY2 = 0D;
            zedGraphControl1.Size = new System.Drawing.Size(653, 343);
            zedGraphControl1.TabIndex = 5;
            zedGraphControl1.GraphPane.XAxis.Type = ZedGraph.AxisType.Linear;
            zedGraphControl1.GraphPane.XAxis.Scale.MaxAuto = true;
            zedGraphControl1.GraphPane.YAxis.Scale.MinAuto = true;
            zedGraphControl1.IsEnableHZoom = false; //垂直方向禁止放大缩小 
            zedGraphControl1.IsEnableVZoom = false; //水平方向禁止放大缩小
            zedGraphControl1.IsEnableHPan = false;
            zedGraphControl1.IsEnableVPan = false;
            zedGraphControl1.GraphPane.GraphObjList.Clear();
            zedGraphControl1.GraphPane.CurveList.Clear();
            myPane1 = zedGraphControl1.GraphPane;
            myPane1.Title.Text = "Pitch";
            myPane1.XAxis.Title.Text = "Time";
            myPane1.YAxis.Title.Text = "Scope";
            myPane1.CurveList.Clear();
            //画图
            list1.Clear();
            myCurve1 = myPane1.AddCurve(Line_name1, list1, Line_color1, SymbolType.None);
            myCurve1.Line.Width = 2.0f;
            ////////////////---------------------------------------------------------
            zedGraphControl2.Name = "zedGraphControl1";
            zedGraphControl2.ScrollGrace = 0D;
            zedGraphControl2.ScrollMaxX = 0D;
            zedGraphControl2.ScrollMaxY = 0D;
            zedGraphControl2.ScrollMaxY2 = 0D;
            zedGraphControl2.ScrollMinX = 0D;
            zedGraphControl2.ScrollMinY = 0D;
            zedGraphControl2.ScrollMinY2 = 0D;
            zedGraphControl2.Size = new System.Drawing.Size(653, 343);
            zedGraphControl2.TabIndex = 5;
            zedGraphControl2.GraphPane.XAxis.Type = ZedGraph.AxisType.Linear;
            zedGraphControl2.GraphPane.XAxis.Scale.MaxAuto = true;
            zedGraphControl2.GraphPane.YAxis.Scale.MinAuto = true;
            zedGraphControl2.IsEnableHZoom = false; //垂直方向禁止放大缩小 
            zedGraphControl2.IsEnableVZoom = false; //水平方向禁止放大缩小
            zedGraphControl2.IsEnableHPan = false;
            zedGraphControl2.IsEnableVPan = false;
            zedGraphControl2.GraphPane.GraphObjList.Clear();
            zedGraphControl2.GraphPane.CurveList.Clear();
            myPane2 = zedGraphControl2.GraphPane;
            myPane2.Title.Text = "Roll";
            myPane2.XAxis.Title.Text = "Time";
            myPane2.YAxis.Title.Text = "Scope";
            myPane2.CurveList.Clear();
            //画图
            list2.Clear();
            myCurve2 = myPane2.AddCurve(Line_name2, list2, Line_color2, SymbolType.None);
            myCurve2.Line.Width = 2.0f;
        }

        void refresh_curve()
        {
            myPane1.CurveList.Clear();
            //画图
            myCurve1 = myPane1.AddCurve(Line_name1, list1, Line_color1, SymbolType.None);
            myCurve1.Line.Width = 1.0f;
            zedGraphControl1.AxisChange();
            zedGraphControl1.Refresh();
            zedGraphControl1.Invalidate();
            ////---------------------------------
            myPane2.CurveList.Clear();
            //画图
            myCurve2 = myPane2.AddCurve(Line_name2, list2, Line_color2, SymbolType.None);
            myCurve2.Line.Width = 1.0f;
            zedGraphControl2.AxisChange();
            zedGraphControl2.Refresh();
            zedGraphControl2.Invalidate();
        }
        #endregion

        private void btn_offset_Click(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                byte[] SendBuf = new byte[100];
                SendBuf[0] = 0xA5;
                SendBuf[1] = 0x5A;
                SendBuf[2] = 0x21;
                SendBuf[3] = 0x05;
                SendBuf[4] = 0xFF;

                serialPort1.Write(SendBuf, 0, 5);
            }
            else
            {
                MessageBox.Show("串口没有打开，请打开串口!");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                byte[] SendBuf = new byte[100];
                SendBuf[0] = 0xA5;
                SendBuf[1] = 0x5A;
                SendBuf[2] = 0x23;
                SendBuf[3] = 0x05;
                SendBuf[4] = 0xFF;

                serialPort1.Write(SendBuf, 0, 5);
            }
            else
            {
                MessageBox.Show("串口没有打开，请打开串口!");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                byte[] SendBuf = new byte[100];
                SendBuf[0] = 0xA5;
                SendBuf[1] = 0x5A;
                SendBuf[2] = 0x22;
                SendBuf[3] = 0x05;
                SendBuf[4] = 0xFF;

                serialPort1.Write(SendBuf, 0, 5);
            }
            else
            {
                MessageBox.Show("串口没有打开，请打开串口!");
            }
        }

        private void Menu_DisableMotor_Click(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                byte[] SendBuf = new byte[100];
                SendBuf[0] = 0xA5;
                SendBuf[1] = 0x5A;
                SendBuf[2] = 0x10;
                SendBuf[3] = 0x05;
                SendBuf[4] = 0xFF;

                serialPort1.Write(SendBuf, 0, 5);
            }
            else
            {
                MessageBox.Show("串口没有打开，请打开串口!");
            }
        }

        private void Menu_EnableMotor_Click(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                byte[] SendBuf = new byte[100];
                SendBuf[0] = 0xA5;
                SendBuf[1] = 0x5A;
                SendBuf[2] = 0x11;
                SendBuf[3] = 0x05;
                SendBuf[4] = 0xFF;

                serialPort1.Write(SendBuf, 0, 5);
            }
            else
            {
                MessageBox.Show("串口没有打开，请打开串口!");
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            zedGraphControl1.AxisChange();
            zedGraphControl2.AxisChange();
            zedGraphControl1.Refresh();
            zedGraphControl2.Refresh();
            zedGraphControl1.Invalidate();
            zedGraphControl2.Invalidate();
        }

        private void btnPitchwrite_Click(object sender, EventArgs e)
        {
            float ep, ip, ii, id;
            if (flagstartup)
            {
                try
                {
                    //根据INI文件名设置要写入INI文件的节点名称
                    //此处的节点名称完全可以根据实际需要进行配置
                    WritePrivateProfileString("PITCHPID", "EPitchP", EPitchP.Value.ToString().Trim(), strFilePath);
                    WritePrivateProfileString("PITCHPID", "IPitchP", IPitchP.Value.ToString().Trim(), strFilePath);
                    WritePrivateProfileString("PITCHPID", "IPitchI", IPitchI.Value.ToString().Trim(), strFilePath);
                    WritePrivateProfileString("PITCHPID", "IPitchD", IPitchD.Value.ToString().Trim(), strFilePath);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message.ToString());
                }
                if (serialPort1.IsOpen)
                {
                    byte[] SendBuf = new byte[100];
                    SendBuf[0] = 0xA5;
                    SendBuf[1] = 0x5A;
                    SendBuf[2] = 0x01;
                    SendBuf[3] = 21;
                    //横滚PID
                    ep = StrToFloat(EPitchP.Value.ToString());
                    (BitConverter.GetBytes(ep)).CopyTo(SendBuf, 4);//float to byte
                    //横滚PID
                    ip = StrToFloat(IPitchP.Value.ToString());
                    (BitConverter.GetBytes(ip)).CopyTo(SendBuf, 8);//float to byte
                    ii = StrToFloat(IPitchI.Value.ToString());
                    (BitConverter.GetBytes(ii)).CopyTo(SendBuf, 12);//float to byte
                    id = StrToFloat(IPitchD.Value.ToString());
                    (BitConverter.GetBytes(id)).CopyTo(SendBuf, 16);//float to byte

                    serialPort1.Write(SendBuf, 0, 21);
                }
                else
                {
                    if (flagstartup)
                        MessageBox.Show("串口没有打开，请打开串口!");
                }
            }
        }

        private void btnRollwrite_Click(object sender, EventArgs e)
        {
            float ep, ip, ii, id;
            if (flagstartup)
            {

                try
                {
                    //根据INI文件名设置要写入INI文件的节点名称
                    //此处的节点名称完全可以根据实际需要进行配置
                    WritePrivateProfileString("ROLLPID", "ERollP", ERollP.Value.ToString().Trim(), strFilePath);
                    WritePrivateProfileString("ROLLPID", "IRollP", IRollP.Value.ToString().Trim(), strFilePath);
                    WritePrivateProfileString("ROLLPID", "IRollI", IRollI.Value.ToString().Trim(), strFilePath);
                    WritePrivateProfileString("ROLLPID", "IRollD", IRollD.Value.ToString().Trim(), strFilePath);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message.ToString());
                }
                if (serialPort1.IsOpen)
                {
                    byte[] SendBuf = new byte[100];
                    SendBuf[0] = 0xA5;
                    SendBuf[1] = 0x5A;
                    SendBuf[2] = 0x02;
                    SendBuf[3] = 21;
                    //横滚PID
                    ep = StrToFloat(ERollP.Value.ToString());
                    (BitConverter.GetBytes(ep)).CopyTo(SendBuf, 4);//float to byte
                    //横滚PID
                    ip = StrToFloat(IRollP.Value.ToString());
                    (BitConverter.GetBytes(ip)).CopyTo(SendBuf, 8);//float to byte
                    ii = StrToFloat(IRollI.Value.ToString());
                    (BitConverter.GetBytes(ii)).CopyTo(SendBuf, 12);//float to byte
                    id = StrToFloat(IRollD.Value.ToString());
                    (BitConverter.GetBytes(id)).CopyTo(SendBuf, 16);//float to byte

                    serialPort1.Write(SendBuf, 0, 21);
                }
                else
                {
                    if (flagstartup)
                        MessageBox.Show("串口没有打开，请打开串口!");
                }
            }
        }

        private void btnYawwrite_Click(object sender, EventArgs e)
        {
            float ep, ip, ii, id;
            if (flagstartup)
            {
                try
                {
                    //根据INI文件名设置要写入INI文件的节点名称
                    //此处的节点名称完全可以根据实际需要进行配置
                    WritePrivateProfileString("YAWPID", "EYawP", EYawP.Value.ToString().Trim(), strFilePath);
                    WritePrivateProfileString("YAWPID", "IYawP", IYawP.Value.ToString().Trim(), strFilePath);
                    WritePrivateProfileString("YAWPID", "IYawI", IYawI.Value.ToString().Trim(), strFilePath);
                    WritePrivateProfileString("YAWPID", "IYawD", IYawD.Value.ToString().Trim(), strFilePath);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message.ToString());
                }
                if (serialPort1.IsOpen)
                {
                    byte[] SendBuf = new byte[100];
                    SendBuf[0] = 0xA5;
                    SendBuf[1] = 0x5A;
                    SendBuf[2] = 0x03;
                    SendBuf[3] = 21;
                    //横滚PID
                    ep = StrToFloat(EYawP.Value.ToString());
                    (BitConverter.GetBytes(ep)).CopyTo(SendBuf, 4);//float to byte
                    //横滚PID
                    ip = StrToFloat(IYawP.Value.ToString());
                    (BitConverter.GetBytes(ip)).CopyTo(SendBuf, 8);//float to byte
                    ii = StrToFloat(IYawI.Value.ToString());
                    (BitConverter.GetBytes(ii)).CopyTo(SendBuf, 12);//float to byte
                    id = StrToFloat(IYawD.Value.ToString());
                    (BitConverter.GetBytes(id)).CopyTo(SendBuf, 16);//float to byte

                    serialPort1.Write(SendBuf, 0, 21);
                }
                else
                {
                    MessageBox.Show("串口没有打开，请打开串口!");
                }
            }
        }
    }
}