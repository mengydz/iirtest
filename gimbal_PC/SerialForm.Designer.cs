﻿namespace SerialDemo
{
    partial class SerialForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SerialForm));
            this.txtRecieve = new System.Windows.Forms.TextBox();
            this.btnClearRead = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.zedGraphControl1 = new ZedGraph.ZedGraphControl();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.Menu_Set = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Set_Comset = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Ctl_Port = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_About = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_DisableMotor = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_EnableMotor = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.Status_Com = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.Status_Lang = new System.Windows.Forms.ToolStripStatusLabel();
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.label9 = new System.Windows.Forms.Label();
            this.ERollP = new System.Windows.Forms.NumericUpDown();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnRollwrite = new System.Windows.Forms.Button();
            this.btnRollread = new System.Windows.Forms.Button();
            this.IRollP = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.IRollD = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.IRollI = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.zedGraphControl2 = new ZedGraph.ZedGraphControl();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btnPitchwrite = new System.Windows.Forms.Button();
            this.btnPitchRead = new System.Windows.Forms.Button();
            this.IPitchP = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.IPitchD = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.IPitchI = new System.Windows.Forms.NumericUpDown();
            this.label12 = new System.Windows.Forms.Label();
            this.EPitchP = new System.Windows.Forms.NumericUpDown();
            this.label13 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.btnYawwrite = new System.Windows.Forms.Button();
            this.btnYawread = new System.Windows.Forms.Button();
            this.IYawP = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.IYawD = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.IYawI = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.EYawP = new System.Windows.Forms.NumericUpDown();
            this.label16 = new System.Windows.Forms.Label();
            this.btn_offset = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ERollP)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.IRollP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IRollD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IRollI)).BeginInit();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.IPitchP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IPitchD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IPitchI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EPitchP)).BeginInit();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.IYawP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IYawD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IYawI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EYawP)).BeginInit();
            this.SuspendLayout();
            // 
            // txtRecieve
            // 
            this.txtRecieve.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRecieve.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtRecieve.Location = new System.Drawing.Point(3, 17);
            this.txtRecieve.Multiline = true;
            this.txtRecieve.Name = "txtRecieve";
            this.txtRecieve.ReadOnly = true;
            this.txtRecieve.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtRecieve.Size = new System.Drawing.Size(1006, 64);
            this.txtRecieve.TabIndex = 0;
            // 
            // btnClearRead
            // 
            this.btnClearRead.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnClearRead.Location = new System.Drawing.Point(1009, 17);
            this.btnClearRead.Name = "btnClearRead";
            this.btnClearRead.Size = new System.Drawing.Size(70, 64);
            this.btnClearRead.TabIndex = 19;
            this.btnClearRead.Text = "清空";
            this.btnClearRead.UseVisualStyleBackColor = true;
            this.btnClearRead.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.zedGraphControl1);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Location = new System.Drawing.Point(3, 17);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(883, 297);
            this.groupBox2.TabIndex = 20;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Pitch";
            // 
            // zedGraphControl1
            // 
            this.zedGraphControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.zedGraphControl1.Location = new System.Drawing.Point(3, 17);
            this.zedGraphControl1.Name = "zedGraphControl1";
            this.zedGraphControl1.ScrollGrace = 0D;
            this.zedGraphControl1.ScrollMaxX = 0D;
            this.zedGraphControl1.ScrollMaxY = 0D;
            this.zedGraphControl1.ScrollMaxY2 = 0D;
            this.zedGraphControl1.ScrollMinX = 0D;
            this.zedGraphControl1.ScrollMinY = 0D;
            this.zedGraphControl1.ScrollMinY2 = 0D;
            this.zedGraphControl1.Size = new System.Drawing.Size(877, 277);
            this.zedGraphControl1.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtRecieve);
            this.groupBox3.Controls.Add(this.btnClearRead);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox3.Location = new System.Drawing.Point(0, 646);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(1082, 84);
            this.groupBox3.TabIndex = 21;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "接收数据";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Menu_Set,
            this.Menu_Ctl_Port,
            this.Menu_About,
            this.Menu_DisableMotor,
            this.Menu_EnableMotor});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1082, 25);
            this.menuStrip1.TabIndex = 22;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // Menu_Set
            // 
            this.Menu_Set.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Menu_Set_Comset});
            this.Menu_Set.Image = global::SerialDemo.Properties.Resources.Customize;
            this.Menu_Set.Name = "Menu_Set";
            this.Menu_Set.Size = new System.Drawing.Size(74, 21);
            this.Menu_Set.Text = "设置(&F)";
            // 
            // Menu_Set_Comset
            // 
            this.Menu_Set_Comset.Image = global::SerialDemo.Properties.Resources.USB;
            this.Menu_Set_Comset.Name = "Menu_Set_Comset";
            this.Menu_Set_Comset.Size = new System.Drawing.Size(140, 22);
            this.Menu_Set_Comset.Text = "端口设置(&C)";
            this.Menu_Set_Comset.Click += new System.EventHandler(this.Menu_Set_Comset_Click);
            // 
            // Menu_Ctl_Port
            // 
            this.Menu_Ctl_Port.Name = "Menu_Ctl_Port";
            this.Menu_Ctl_Port.Size = new System.Drawing.Size(68, 21);
            this.Menu_Ctl_Port.Text = "打开串口";
            this.Menu_Ctl_Port.Click += new System.EventHandler(this.Menu_Ctl_Port_Click);
            // 
            // Menu_About
            // 
            this.Menu_About.Image = global::SerialDemo.Properties.Resources.Get_Info;
            this.Menu_About.Name = "Menu_About";
            this.Menu_About.Size = new System.Drawing.Size(76, 21);
            this.Menu_About.Text = "关于(&A)";
            this.Menu_About.Click += new System.EventHandler(this.Menu_About_Click);
            // 
            // Menu_DisableMotor
            // 
            this.Menu_DisableMotor.Name = "Menu_DisableMotor";
            this.Menu_DisableMotor.Size = new System.Drawing.Size(68, 21);
            this.Menu_DisableMotor.Text = "失能电机";
            this.Menu_DisableMotor.Click += new System.EventHandler(this.Menu_DisableMotor_Click);
            // 
            // Menu_EnableMotor
            // 
            this.Menu_EnableMotor.Name = "Menu_EnableMotor";
            this.Menu_EnableMotor.Size = new System.Drawing.Size(68, 21);
            this.Menu_EnableMotor.Text = "使能电机";
            this.Menu_EnableMotor.Click += new System.EventHandler(this.Menu_EnableMotor_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Status_Com,
            this.toolStripStatusLabel1,
            this.Status_Lang});
            this.statusStrip1.Location = new System.Drawing.Point(0, 730);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1082, 22);
            this.statusStrip1.TabIndex = 23;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // Status_Com
            // 
            this.Status_Com.Name = "Status_Com";
            this.Status_Com.Size = new System.Drawing.Size(56, 17);
            this.Status_Com.Text = "串口关闭";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(947, 17);
            this.toolStripStatusLabel1.Spring = true;
            this.toolStripStatusLabel1.Text = "           ";
            // 
            // Status_Lang
            // 
            this.Status_Lang.Name = "Status_Lang";
            this.Status_Lang.Size = new System.Drawing.Size(64, 17);
            this.Status_Lang.Text = "中文(中国)";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(17, 22);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(23, 12);
            this.label9.TabIndex = 30;
            this.label9.Text = "外P";
            // 
            // ERollP
            // 
            this.ERollP.DecimalPlaces = 2;
            this.ERollP.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.ERollP.Location = new System.Drawing.Point(42, 20);
            this.ERollP.Name = "ERollP";
            this.ERollP.Size = new System.Drawing.Size(64, 21);
            this.ERollP.TabIndex = 31;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnRollwrite);
            this.groupBox1.Controls.Add(this.btnRollread);
            this.groupBox1.Controls.Add(this.IRollP);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.IRollD);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.IRollI);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.ERollP);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Location = new System.Drawing.Point(898, 230);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(126, 196);
            this.groupBox1.TabIndex = 34;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "PID(Roll)";
            // 
            // btnRollwrite
            // 
            this.btnRollwrite.Location = new System.Drawing.Point(19, 167);
            this.btnRollwrite.Name = "btnRollwrite";
            this.btnRollwrite.Size = new System.Drawing.Size(87, 23);
            this.btnRollwrite.TabIndex = 43;
            this.btnRollwrite.Text = "写入";
            this.btnRollwrite.UseVisualStyleBackColor = true;
            this.btnRollwrite.Click += new System.EventHandler(this.btnRollwrite_Click);
            // 
            // btnRollread
            // 
            this.btnRollread.Location = new System.Drawing.Point(19, 138);
            this.btnRollread.Name = "btnRollread";
            this.btnRollread.Size = new System.Drawing.Size(87, 23);
            this.btnRollread.TabIndex = 42;
            this.btnRollread.Text = "读取";
            this.btnRollread.UseVisualStyleBackColor = true;
            // 
            // IRollP
            // 
            this.IRollP.DecimalPlaces = 2;
            this.IRollP.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.IRollP.Location = new System.Drawing.Point(42, 47);
            this.IRollP.Name = "IRollP";
            this.IRollP.Size = new System.Drawing.Size(64, 21);
            this.IRollP.TabIndex = 37;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(17, 49);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(23, 12);
            this.label4.TabIndex = 36;
            this.label4.Text = "内P";
            // 
            // IRollD
            // 
            this.IRollD.DecimalPlaces = 2;
            this.IRollD.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.IRollD.Location = new System.Drawing.Point(42, 105);
            this.IRollD.Name = "IRollD";
            this.IRollD.Size = new System.Drawing.Size(64, 21);
            this.IRollD.TabIndex = 39;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(17, 107);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(23, 12);
            this.label5.TabIndex = 35;
            this.label5.Text = "内D";
            // 
            // IRollI
            // 
            this.IRollI.DecimalPlaces = 2;
            this.IRollI.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.IRollI.Location = new System.Drawing.Point(42, 76);
            this.IRollI.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.IRollI.Name = "IRollI";
            this.IRollI.Size = new System.Drawing.Size(64, 21);
            this.IRollI.TabIndex = 38;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(17, 78);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(23, 12);
            this.label6.TabIndex = 34;
            this.label6.Text = "内I";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.zedGraphControl2);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox5.Location = new System.Drawing.Point(3, 314);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(883, 292);
            this.groupBox5.TabIndex = 36;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Roll";
            // 
            // zedGraphControl2
            // 
            this.zedGraphControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.zedGraphControl2.Location = new System.Drawing.Point(3, 17);
            this.zedGraphControl2.Name = "zedGraphControl2";
            this.zedGraphControl2.ScrollGrace = 0D;
            this.zedGraphControl2.ScrollMaxX = 0D;
            this.zedGraphControl2.ScrollMaxY = 0D;
            this.zedGraphControl2.ScrollMaxY2 = 0D;
            this.zedGraphControl2.ScrollMinX = 0D;
            this.zedGraphControl2.ScrollMinY = 0D;
            this.zedGraphControl2.ScrollMinY2 = 0D;
            this.zedGraphControl2.Size = new System.Drawing.Size(877, 272);
            this.zedGraphControl2.TabIndex = 1;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.groupBox5);
            this.groupBox6.Controls.Add(this.groupBox2);
            this.groupBox6.Location = new System.Drawing.Point(3, 28);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(889, 612);
            this.groupBox6.TabIndex = 37;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "实时曲线";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btnPitchwrite);
            this.groupBox4.Controls.Add(this.btnPitchRead);
            this.groupBox4.Controls.Add(this.IPitchP);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this.IPitchD);
            this.groupBox4.Controls.Add(this.label11);
            this.groupBox4.Controls.Add(this.IPitchI);
            this.groupBox4.Controls.Add(this.label12);
            this.groupBox4.Controls.Add(this.EPitchP);
            this.groupBox4.Controls.Add(this.label13);
            this.groupBox4.Location = new System.Drawing.Point(898, 28);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(126, 196);
            this.groupBox4.TabIndex = 39;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "PID(Pitch)";
            // 
            // btnPitchwrite
            // 
            this.btnPitchwrite.Location = new System.Drawing.Point(19, 161);
            this.btnPitchwrite.Name = "btnPitchwrite";
            this.btnPitchwrite.Size = new System.Drawing.Size(87, 23);
            this.btnPitchwrite.TabIndex = 41;
            this.btnPitchwrite.Text = "写入";
            this.btnPitchwrite.UseVisualStyleBackColor = true;
            this.btnPitchwrite.Click += new System.EventHandler(this.btnPitchwrite_Click);
            // 
            // btnPitchRead
            // 
            this.btnPitchRead.Location = new System.Drawing.Point(19, 132);
            this.btnPitchRead.Name = "btnPitchRead";
            this.btnPitchRead.Size = new System.Drawing.Size(87, 23);
            this.btnPitchRead.TabIndex = 40;
            this.btnPitchRead.Text = "读取";
            this.btnPitchRead.UseVisualStyleBackColor = true;
            // 
            // IPitchP
            // 
            this.IPitchP.DecimalPlaces = 2;
            this.IPitchP.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.IPitchP.Location = new System.Drawing.Point(42, 47);
            this.IPitchP.Name = "IPitchP";
            this.IPitchP.Size = new System.Drawing.Size(64, 21);
            this.IPitchP.TabIndex = 37;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(17, 49);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(23, 12);
            this.label10.TabIndex = 36;
            this.label10.Text = "内P";
            // 
            // IPitchD
            // 
            this.IPitchD.DecimalPlaces = 2;
            this.IPitchD.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.IPitchD.Location = new System.Drawing.Point(42, 105);
            this.IPitchD.Name = "IPitchD";
            this.IPitchD.Size = new System.Drawing.Size(64, 21);
            this.IPitchD.TabIndex = 39;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(17, 107);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(23, 12);
            this.label11.TabIndex = 35;
            this.label11.Text = "内D";
            // 
            // IPitchI
            // 
            this.IPitchI.DecimalPlaces = 2;
            this.IPitchI.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.IPitchI.Location = new System.Drawing.Point(42, 76);
            this.IPitchI.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.IPitchI.Name = "IPitchI";
            this.IPitchI.Size = new System.Drawing.Size(64, 21);
            this.IPitchI.TabIndex = 38;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(17, 78);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(23, 12);
            this.label12.TabIndex = 34;
            this.label12.Text = "内I";
            // 
            // EPitchP
            // 
            this.EPitchP.DecimalPlaces = 2;
            this.EPitchP.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.EPitchP.Location = new System.Drawing.Point(42, 20);
            this.EPitchP.Name = "EPitchP";
            this.EPitchP.Size = new System.Drawing.Size(64, 21);
            this.EPitchP.TabIndex = 31;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(17, 22);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(23, 12);
            this.label13.TabIndex = 30;
            this.label13.Text = "外P";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.btnYawwrite);
            this.groupBox7.Controls.Add(this.btnYawread);
            this.groupBox7.Controls.Add(this.IYawP);
            this.groupBox7.Controls.Add(this.label1);
            this.groupBox7.Controls.Add(this.IYawD);
            this.groupBox7.Controls.Add(this.label2);
            this.groupBox7.Controls.Add(this.IYawI);
            this.groupBox7.Controls.Add(this.label3);
            this.groupBox7.Controls.Add(this.EYawP);
            this.groupBox7.Controls.Add(this.label16);
            this.groupBox7.Location = new System.Drawing.Point(898, 432);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(126, 196);
            this.groupBox7.TabIndex = 40;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "PID(YAW)";
            // 
            // btnYawwrite
            // 
            this.btnYawwrite.Location = new System.Drawing.Point(19, 167);
            this.btnYawwrite.Name = "btnYawwrite";
            this.btnYawwrite.Size = new System.Drawing.Size(87, 23);
            this.btnYawwrite.TabIndex = 45;
            this.btnYawwrite.Text = "写入";
            this.btnYawwrite.UseVisualStyleBackColor = true;
            this.btnYawwrite.Click += new System.EventHandler(this.btnYawwrite_Click);
            // 
            // btnYawread
            // 
            this.btnYawread.Location = new System.Drawing.Point(19, 138);
            this.btnYawread.Name = "btnYawread";
            this.btnYawread.Size = new System.Drawing.Size(87, 23);
            this.btnYawread.TabIndex = 44;
            this.btnYawread.Text = "读取";
            this.btnYawread.UseVisualStyleBackColor = true;
            // 
            // IYawP
            // 
            this.IYawP.DecimalPlaces = 2;
            this.IYawP.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.IYawP.Location = new System.Drawing.Point(42, 47);
            this.IYawP.Name = "IYawP";
            this.IYawP.Size = new System.Drawing.Size(64, 21);
            this.IYawP.TabIndex = 37;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(23, 12);
            this.label1.TabIndex = 36;
            this.label1.Text = "内P";
            // 
            // IYawD
            // 
            this.IYawD.DecimalPlaces = 2;
            this.IYawD.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.IYawD.Location = new System.Drawing.Point(42, 105);
            this.IYawD.Name = "IYawD";
            this.IYawD.Size = new System.Drawing.Size(64, 21);
            this.IYawD.TabIndex = 39;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 107);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(23, 12);
            this.label2.TabIndex = 35;
            this.label2.Text = "内D";
            // 
            // IYawI
            // 
            this.IYawI.DecimalPlaces = 2;
            this.IYawI.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.IYawI.Location = new System.Drawing.Point(42, 76);
            this.IYawI.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.IYawI.Name = "IYawI";
            this.IYawI.Size = new System.Drawing.Size(64, 21);
            this.IYawI.TabIndex = 38;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(17, 78);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(23, 12);
            this.label3.TabIndex = 34;
            this.label3.Text = "内I";
            // 
            // EYawP
            // 
            this.EYawP.DecimalPlaces = 2;
            this.EYawP.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.EYawP.Location = new System.Drawing.Point(42, 20);
            this.EYawP.Name = "EYawP";
            this.EYawP.Size = new System.Drawing.Size(64, 21);
            this.EYawP.TabIndex = 31;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(17, 22);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(23, 12);
            this.label16.TabIndex = 30;
            this.label16.Text = "外P";
            // 
            // btn_offset
            // 
            this.btn_offset.Location = new System.Drawing.Point(1030, 34);
            this.btn_offset.Name = "btn_offset";
            this.btn_offset.Size = new System.Drawing.Size(30, 190);
            this.btn_offset.TabIndex = 41;
            this.btn_offset.Text = "水  平  校  准";
            this.btn_offset.UseVisualStyleBackColor = true;
            this.btn_offset.Click += new System.EventHandler(this.btn_offset_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(1030, 438);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(30, 190);
            this.button1.TabIndex = 42;
            this.button1.Text = "3#  零  位  校  准";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(1030, 236);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(30, 190);
            this.button2.TabIndex = 43;
            this.button2.Text = "2#  零  位  校  准";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // SerialForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1082, 752);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btn_offset);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SerialForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "云台调试助手V1.0---by陈梦洋";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SerialForm_FormClosing);
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ERollP)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.IRollP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IRollD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IRollI)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.IPitchP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IPitchD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IPitchI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EPitchP)).EndInit();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.IYawP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IYawD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IYawI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EYawP)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtRecieve;
        private System.Windows.Forms.Button btnClearRead;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem Menu_Set;
        private System.Windows.Forms.ToolStripMenuItem Menu_Set_Comset;
        private System.Windows.Forms.ToolStripMenuItem Menu_About;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel Status_Com;
        private System.Windows.Forms.ToolStripStatusLabel Status_Lang;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.IO.Ports.SerialPort serialPort1;
        private System.Windows.Forms.ToolStripMenuItem Menu_Ctl_Port;
        private ZedGraph.ZedGraphControl zedGraphControl1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown ERollP;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox6;
        private ZedGraph.ZedGraphControl zedGraphControl2;
        private System.Windows.Forms.NumericUpDown IRollP;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown IRollD;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown IRollI;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.NumericUpDown IPitchP;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown IPitchD;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.NumericUpDown IPitchI;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.NumericUpDown EPitchP;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.NumericUpDown IYawP;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown IYawD;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown IYawI;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown EYawP;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button btn_offset;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ToolStripMenuItem Menu_DisableMotor;
        private System.Windows.Forms.ToolStripMenuItem Menu_EnableMotor;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button btnRollwrite;
        private System.Windows.Forms.Button btnRollread;
        private System.Windows.Forms.Button btnPitchwrite;
        private System.Windows.Forms.Button btnPitchRead;
        private System.Windows.Forms.Button btnYawwrite;
        private System.Windows.Forms.Button btnYawread;
    }
}

