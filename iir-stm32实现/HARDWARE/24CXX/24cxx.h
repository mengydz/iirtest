#ifndef __24CXX_H
#define __24CXX_H
#include "sys.h"
#include "ANO_TC_STM32F1_I2C.h"

#define AT24C01		127
#define AT24C02		255
#define AT24C04		511
#define AT24C08		1023
#define AT24C16		2047
#define AT24C32		4095
#define AT24C64	    8191
#define AT24C128	16383
#define AT24C256	32767  
//Mini STM32开发板使用的是24c02，所以定义EE_TYPE为AT24C02
#define EE_TYPE AT24C02
#define AddrAT24Cxx	0xA0

void AT24CXX_Write(u16 WriteAddr,u16 NumToWrite,u8 *pBuffer);	//从指定地址开始写入指定长度的数据
void AT24CXX_Read(u16 ReadAddr,u16 NumToRead,u8 *pBuffer);   	//从指定地址开始读出指定长度的数据

u8 AT24CXX_Check(void);  //检查器件
#endif
















