#include "24cxx.h" 


void AT24CXX_Write(u16 WriteAddr,u16 NumToWrite,u8 *pBuffer)	//从指定地址开始写入指定长度的数据
{
	ANO_TC_I2C2_Write_Buf(AddrAT24Cxx,WriteAddr,NumToWrite,pBuffer);
}

void AT24CXX_Read(u16 ReadAddr,u16 NumToRead,u8 *pBuffer)   	//从指定地址开始读出指定长度的数据
{
	ANO_TC_I2C2_Read_Buf(AddrAT24Cxx,ReadAddr,NumToRead,pBuffer);
}

