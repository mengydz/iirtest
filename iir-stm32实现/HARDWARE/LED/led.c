#include "led.h"


//初始化PB5和PE5为输出口.并使能这两个口的时钟		    
//LED IO初始化
void LED_Init(void)
{
 
 GPIO_InitTypeDef  GPIO_InitStructure;
 	
 RCC_APB2PeriphClockCmd(LED0_CLOCK, ENABLE);	 //使能PB端口时钟
	
 GPIO_InitStructure.GPIO_Pin = LED0_Pin;				 //LED0-->PB.8 端口配置
 GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 		 //推挽输出
 GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;		 //IO口速度为50MHz
 GPIO_Init(LED0_GPIO, &GPIO_InitStructure);					 //根据设定参数初始化GPIOB.8
 GPIO_SetBits(LED0_GPIO,LED0_Pin);						 //PB.8 输出高
}
 
 
