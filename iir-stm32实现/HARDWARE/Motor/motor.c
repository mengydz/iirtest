#include "motor.h"
#include <math.h>
#include "UserMath.h"

u8 TIM1CH1_CAPTURE_STA = 0;	//通道1输入捕获标志，高两位做捕获标志，低6位做溢出标志		
u16 TIM1CH1_CAPTURE_UPVAL;
u16 LAST_TIM1CH1_CAPTURE_UPVAL;
u16 TIM1CH1_CAPTURE_DOWNVAL;
u32 cappwmperiod;
s32 tempup1 = 0;	//捕获总高电平的时间
uint8_t CaptureUpdate=0;
float StartPlus;
float StopPlus;

int16_t MotorSpeed;
uint16_t PositionZeroMagnet;
uint16_t PositionZeroMotor;
float reasmotorangle;
uint8_t MotorEnable = 1;
uint16_t MotorPower;

void TIM1_Cap_Init(u16 arr, u16 psc)
{
	TIM_ICInitTypeDef TIM1_ICInitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;
	TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1, ENABLE);	//使能TIM4时钟
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);  //使能GPIOB时钟

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8;  //PB8 清除之前设置  
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; //IO速率50MHz
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPD; //PB8 输入 
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	//初始化定时器1 TIM4	 
	TIM_TimeBaseStructure.TIM_Period = arr; //设定计数器自动重装值 
	TIM_TimeBaseStructure.TIM_Prescaler = psc; 	//预分频器 
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1; //设置时钟分割:TDTS = Tck_tim
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;  //TIM向上计数模式
	TIM_TimeBaseInit(TIM1, &TIM_TimeBaseStructure); //根据TIM_TimeBaseInitStruct中指定的参数初始化TIMx的时间基数单位

	//初始化TIM1输入捕获参数 通道1
	TIM1_ICInitStructure.TIM_Channel = TIM_Channel_1; //CC1S=01 	选择输入端 IC1映射到TI1上
	TIM1_ICInitStructure.TIM_ICPolarity = TIM_ICPolarity_Rising;	//上升沿捕获
	TIM1_ICInitStructure.TIM_ICSelection = TIM_ICSelection_DirectTI; //映射到TI1上
	TIM1_ICInitStructure.TIM_ICPrescaler = TIM_ICPSC_DIV1;	  //配置输入分频,不分频
	TIM1_ICInitStructure.TIM_ICFilter = 0x00;	  //IC1F=0000 配置输入滤波器 不滤波
	TIM_ICInit(TIM1, &TIM1_ICInitStructure);

	//中断分组初始化
	NVIC_InitStructure.NVIC_IRQChannel = TIM1_CC_IRQn;  //TIM1中断
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;  //先占优先级1级
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;  //从优先级0级
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE; //IRQ通道被使能
	NVIC_Init(&NVIC_InitStructure);   //根据NVIC_InitStruct中指定的参数初始化外设NVIC寄存器 

	TIM_ITConfig(TIM1, TIM_IT_CC1,ENABLE);   //不允许更新中断，允许CC1IE捕获中断	

	TIM_Cmd(TIM1, ENABLE); 		//使能定时器4
}

void TIM2_Motor_Init(u16 arr,u16 psc)//Driver Timer configuration
{	

	GPIO_InitTypeDef        		GPIO_InitStructure;
	TIM_OCInitTypeDef       		TIM_OCInitStructure;
	TIM_TimeBaseInitTypeDef 		TIM_TimeBaseInitStructure;

	//Timer1 Config
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA|RCC_APB2Periph_AFIO, ENABLE);	 //使能PB端口时钟
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1|GPIO_Pin_2|GPIO_Pin_3;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	//Time Base configuration 
	TIM_TimeBaseInitStructure.TIM_Prescaler =psc; // Period*Prescaler=24'000'000Hz  //2400 1s
	TIM_TimeBaseInitStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInitStructure.TIM_Period = arr; //20000(presc=24)=50hz(servo signal)
	TIM_TimeBaseInitStructure.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseInit(TIM2, &TIM_TimeBaseInitStructure);		

	//Configuration in PWM mode
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1 ;
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_Pulse = 0;      
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
	TIM_OCInitStructure.TIM_OCIdleState = TIM_OCIdleState_Set;
	TIM_OC2Init(TIM2, &TIM_OCInitStructure);	
	TIM_OC3Init(TIM2, &TIM_OCInitStructure);	
	TIM_OC4Init(TIM2, &TIM_OCInitStructure);	

	//TIM2 counter enable
	TIM_Cmd(TIM2, ENABLE);

	//Main Output Enable
	TIM_CtrlPWMOutputs(TIM2, ENABLE);
}

//定时器1中断服务程序
void TIM1_CC_IRQHandler(void)
{

	if ((TIM1CH1_CAPTURE_STA & 0X80) == 0) 		//还未成功捕获	
	{
		if (TIM_GetITStatus(TIM1, TIM_IT_CC1) != RESET) 		//捕获1发生捕获事件
		{
			TIM_ClearITPendingBit(TIM1, TIM_IT_CC1); 		//清除中断标志位
			if (TIM1CH1_CAPTURE_STA & 0X40)		//捕获到一个下降沿
			{
				TIM1CH1_CAPTURE_DOWNVAL = TIM_GetCapture1(TIM1);//记录下此时的定时器计数值
				if (TIM1CH1_CAPTURE_DOWNVAL < TIM1CH1_CAPTURE_UPVAL)
				{
					tempup1 = TIM1CH1_CAPTURE_DOWNVAL - TIM1CH1_CAPTURE_UPVAL + 65535;		//得到总的高电平的时间
				}
				else
					tempup1 = TIM1CH1_CAPTURE_DOWNVAL - TIM1CH1_CAPTURE_UPVAL + 0;		//得到总的高电平的时间
				CaptureUpdate = 1;
				TIM1CH1_CAPTURE_STA = 0;//捕获标志位清零
				TIM_OC1PolarityConfig(TIM1, TIM_ICPolarity_Rising); //设置为上升沿捕获		  
			}
			else //发生捕获时间但不是下降沿，第一次捕获到上升沿，记录此时的定时器计数值
			{
				TIM1CH1_CAPTURE_UPVAL = TIM_GetCapture1(TIM1);		//获取上升沿数据
				TIM1CH1_CAPTURE_STA |= 0X40;						//标记已捕获到上升沿
				if (TIM1CH1_CAPTURE_UPVAL < LAST_TIM1CH1_CAPTURE_UPVAL)
					cappwmperiod = TIM1CH1_CAPTURE_UPVAL - LAST_TIM1CH1_CAPTURE_UPVAL+65535;
				else
					cappwmperiod = TIM1CH1_CAPTURE_UPVAL - LAST_TIM1CH1_CAPTURE_UPVAL;
				LAST_TIM1CH1_CAPTURE_UPVAL = TIM1CH1_CAPTURE_UPVAL;
				TIM_OC1PolarityConfig(TIM1, TIM_ICPolarity_Falling);//设置为下降沿捕获
			}
		}
	}
}

void MotorTest(float dat)
{
	TIM2->CCR2=(sin(dat)	      * 600)+1000;//+
	TIM2->CCR3=(sin(dat+M_2_PI_3) * 600)+1000;
	TIM2->CCR4=(sin(dat+M_4_PI_3) * 600)+1000;
}

void MotorDriver(void)
{
	int16_t tempencoder;
	float DivElePlus = (StopPlus-StartPlus)/PolePairs/2.0f;
	if(tempup1 - PositionZeroMagnet<0)
		tempencoder = tempup1 + (StopPlus - StartPlus) - PositionZeroMagnet;
	else
		tempencoder = tempup1 - PositionZeroMagnet;
	
	if(MotorSpeed>=950) MotorSpeed = 950;
	else if(MotorSpeed<=-950) MotorSpeed = -950;
	if(MotorEnable)
	{
		TIM2->CCR2=(FastSin((-tempencoder/DivElePlus+0.5f) * M_PI	       ) * MotorSpeed)+1000;//
		TIM2->CCR3=(FastSin((-tempencoder/DivElePlus+0.5f) * M_PI + M_2_PI_3) * MotorSpeed)+1000;
		TIM2->CCR4=(FastSin((-tempencoder/DivElePlus+0.5f) * M_PI + M_4_PI_3) * MotorSpeed)+1000;
	}
}

float PitchMotorAngle[15];
uint8_t MotorPitch_cnt=0;
float GetMotorAnglePosition(void)
{
	int16_t tempencoder;
	
	if(tempup1 - PositionZeroMotor<0)
		tempencoder = tempup1 - PositionZeroMotor + (StopPlus - StartPlus);
	else
		tempencoder = tempup1 - PositionZeroMotor;
	float _motorposition = ((float)tempencoder/(StopPlus - StartPlus))*360.0;
	return _motorposition;// = _motorpositionold *0.5 + 0.5*_motorposition;
}

