#ifndef __MOTOR_H
#define __MOTOR_H
#include "sys.h"

#define LowPlus 0.0294185244771317	//128.0f/4351.0f;
#define HighPlus 0.9705814755228683//	4223.0f/4351.0f;
#define PolePairs 4

void TIM1_Cap_Init(u16 arr, u16 psc);

void TIM2_Motor_Init(u16 arr,u16 psc);//Driver Timer configuration
void MotorTest(float dat);
void MotorDriver(void);
float GetMotorAnglePosition(void);

extern float StartPlus;
extern float StopPlus;
extern uint8_t CaptureUpdate;
extern s32 tempup1;	//捕获总高电平的时间
extern u32 cappwmperiod;
extern int16_t MotorSpeed;
extern uint16_t PositionZeroMagnet;
extern uint16_t PositionZeroMotor;
extern float reasmotorangle;
extern uint8_t MotorEnable;
extern uint16_t MotorPower;

#endif
