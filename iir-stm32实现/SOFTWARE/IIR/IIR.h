#ifndef __IIR_H
#define __IIR_H
#include "math.h"

#define     PI     ((double)3.1415926)
#define 		TAR_COFOFF_BAND		1
#define 		TAR_STOP_BAND			8
#define 		STOP_ATTENUATION	60
#define 		SAMPLE_FREQUENCY	100

#define 		COFOFF_BAND		(double)(2*tan((2*PI/((double)SAMPLE_FREQUENCY/(double)TAR_COFOFF_BAND))/2))
#define 		STOP_BAND			(double)(2*tan((2*PI/((double)SAMPLE_FREQUENCY/(double)TAR_STOP_BAND))/2))

#define 		M							(int)(ceil(0.5*( log10 ( pow (10, (double)STOP_ATTENUATION/10) - 1) / log10 ((double)STOP_BAND/(double)COFOFF_BAND))))

typedef struct 
{
	double Real_part;
	double Imag_Part;
} COMPLEX;

typedef struct 
{
	double Cotoff;   
	double Stopband;
	double Stopband_attenuation;
}DESIGN_SPECIFICATION;

double IIRFilter  (double *a, int Lenth_a,double *b, int Lenth_b,double Input_Data,double *Memory_Buffer);
int Direct( double Cotoff,double Stopband,double Stopband_attenuation,int N,double *az,double *bz);

#endif
