#include "PID.h"
#include "UserMath.h"

PID Pid_Pitch;
PID Gain_Pitch;

double  PIDCalc( PID *pp, double NextPoint , double imax)   
{   
	double dError,LastError,kp_output,ki_output,kd_output,pid_output;

	LastError = pp->SetPoint -  NextPoint;      // 当前偏差   
	kp_output = pp->Proportion * LastError;     // 比例项 
	
	pp->SumError += pp->Integral * LastError;   // 积分 
	pp->SumError = Math_fConstrain(pp->SumError,-imax,imax);
	ki_output = pp->SumError;   								// 积分项
	
	dError = LastError - pp->PrevError;     // 当前微分   
	kd_output = pp->Derivative * dError;        // 微分项   
	pp->PrevError = LastError;   

  pid_output = kp_output+ki_output+kd_output;
	pid_output = Math_fConstrain(pid_output,-imax,imax);
	return pid_output;
}              
void PIDInit (PID *pp)   
{   
	memset ( pp,0,sizeof(PID));   		//memset函数清零初始化
}         

