#ifndef __PID_H
#define __PID_H	 
#include "sys.h"

typedef struct PID {   

    double  SetPoint;           //  设定目标 Desired Value 
	  
    double  Proportion;         //  比例常数 Proportional Const   
    double  Integral;           //  积分常数 Integral Const   
    double  Derivative;         //  微分常数 Derivative Const   
		
    double  LastError;          //  Error[0]
    double  PrevError;          //  Error[-1]  
    double  SumError;           //  Sums of Errors   

} PID;       

void PIDInit (PID *pp);   
double  PIDCalc( PID *pp, double NextPoint , double imax);  
extern PID Pid_Pitch;
extern PID Gain_Pitch;
#endif
