#include "UserMath.h"
#include <sine.h>


float Math_fConstrain(float value, float min, float max){
if(value > max)value = max;
	else if(value < min)value = min;
return value;
}

u8 get_add_check(u8* bbf,u8 start,u8 end)
{
	u8 xxi=0;
	u8 sum_check=0;
	for(xxi=start;xxi<end;xxi++)
	{
		sum_check+=bbf[xxi];
	}
  return sum_check;
}

u8 BCC_Check(u8 start, u8* data)//BCCУ��
{
	u8 bccData = 0;
	u8 i;
	for (i = start; i < 8; i++)
	{
		bccData ^= data[i - 1];
	}
	return bccData;
}

float move_average_filtaer(float *data_buf, float data , uint8_t *number,uint8_t max)
{
    double temp_value=0;
	uint8_t i;
	data_buf[*number] = data;
	if(++(*number) >= max)
		*number = 0;
	
	for(i=0;i<max;i++)
	{
		temp_value += data_buf[i];
	}
	temp_value /= max;
    return temp_value;
}

float FastSin(float dat)
{
	int16_t Angle10 = dat * 57.324841f * 10;

	while(Angle10>3600)
	{
		Angle10 -= 3600;
	}
	while(Angle10<0)
	{
		Angle10 += 3600;
	}

	if( Angle10 < 900 && Angle10 >= 0 ) return sin_data[Angle10];
	else if( Angle10 < 1800 && Angle10 >= 900 )
	{
		Angle10 = 1800 - Angle10;
		return sin_data[Angle10];
	}
	else if( Angle10 < 2700 && Angle10 >= 1800 )
	{
		Angle10 = Angle10 - 1800;
		return -sin_data[Angle10];
	}
	else if( Angle10 < 3600 && Angle10 >= 2700 )
	{
		Angle10 = 3600 - Angle10;
		return -sin_data[Angle10];
	}
	else return 0;
}
	

/***************************END OF FILE**********************************************************************/
