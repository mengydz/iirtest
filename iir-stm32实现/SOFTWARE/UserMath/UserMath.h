#ifndef __USERMATH_H_
#define __USERMATH_H_
#include "sys.h"

#define M_PI       3.14159265358979323846  /* pi 	*/
#define M_PI_2     1.57079632679489661923  /* pi/2 */
#define M_PI_4     0.78539816339744830962  /* pi/4 */
#define M_2_PI     6.283185307179586  		/* 2*pi */
#define M_2_PI_3   2.094395102393195  		/* 2*pi */
#define M_4_PI_3   4.18879020478639  		/* 5*pi */

float Math_fConstrain(float value, float min, float max);
u8 get_add_check(u8* bbf,u8 start,u8 end);
u8 BCC_Check(u8 start, u8* data);//BCCУ��
float move_average_filtaer(float *data_buf, float data , uint8_t *number , uint8_t max);
float FastSin(float dat);
#endif
