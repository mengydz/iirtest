#ifndef _UPROTOCOL_H
#define _UPROTOCOL_H
#include "sys.h"

#define Proto_HEAD1 	0XAA
#define Proto_HEAD2 	0XBB

uint8_t Upload_data(void);  //return tx size
uint8_t Upload_Gcs_data(void);  //return tx size
void SetMotorSpeed(int dat);

#endif


