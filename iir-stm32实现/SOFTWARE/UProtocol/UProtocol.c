#include "uprotocol.h"
#include "usart.h"	  
#include "motor.h"
#include "UserMath.h"
#include "mpu6050.h"
#include "imu.h"
////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////开发者////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
uint8_t Upload_data(void)  //return tx size
{
	uint8_t bufid=0;
	uint8_t length_buf_id=0;
	floatData fvalue;

	
	USART1_TX_DATA[bufid++]=Proto_HEAD1;			//HEAD 一样
	USART1_TX_DATA[bufid++]=Proto_HEAD2;			//包头
	USART1_TX_DATA[bufid++]=0x01;//length

	USART1_TX_DATA[bufid++]=0;// length  (93-5+1)  +1 check   //不填写，最后更新
	length_buf_id = bufid-1;
	
	fvalue.fData = imupitch;//Pitch角度
	USART1_TX_DATA[bufid++]=fvalue.cData[0];
	USART1_TX_DATA[bufid++]=fvalue.cData[1];
	USART1_TX_DATA[bufid++]=fvalue.cData[2];
	USART1_TX_DATA[bufid++]=fvalue.cData[3];

	fvalue.fData = imuroll;//Roll角度
	USART1_TX_DATA[bufid++]=fvalue.cData[0];
	USART1_TX_DATA[bufid++]=fvalue.cData[1];
	USART1_TX_DATA[bufid++]=fvalue.cData[2];
	USART1_TX_DATA[bufid++]=fvalue.cData[3];

	fvalue.fData = AveMpu.gyry;//Roll轴角速度
	USART1_TX_DATA[bufid++]=fvalue.cData[0];
	USART1_TX_DATA[bufid++]=fvalue.cData[1];
	USART1_TX_DATA[bufid++]=fvalue.cData[2];
	USART1_TX_DATA[bufid++]=fvalue.cData[3];

	fvalue.fData = AveMpu.gyrz;//Yaw轴角速度
	USART1_TX_DATA[bufid++]=fvalue.cData[0];
	USART1_TX_DATA[bufid++]=fvalue.cData[1];
	USART1_TX_DATA[bufid++]=fvalue.cData[2];
	USART1_TX_DATA[bufid++]=fvalue.cData[3];

	fvalue.fData = reasmotorangle;//电机角度
	USART1_TX_DATA[bufid++]=fvalue.cData[0];
	USART1_TX_DATA[bufid++]=fvalue.cData[1];
	USART1_TX_DATA[bufid++]=fvalue.cData[2];
	USART1_TX_DATA[bufid++]=fvalue.cData[3];

	USART1_TX_DATA[bufid]=get_add_check(USART1_TX_DATA,4,bufid-1);  //加和校验 校验到前一个字节				
	USART1_TX_DATA[length_buf_id]=(bufid+1)/*总字节*/-4/*HEAD&ID&LENGTH*/-1/*校验位*/;  //已包括校验位////更新长度字节

	Uart1Sends(bufid+1);  //发送

	return bufid;
}

extern float sss,iirsss,oldsss;
uint8_t Upload_Gcs_data(void)  //return tx size
{
	uint8_t bufid=0;
	uint8_t length_buf_id=0;
	floatData fvalue;

	
	USART1_TX_DATA[bufid++]=Proto_HEAD1;			//HEAD 一样
	USART1_TX_DATA[bufid++]=Proto_HEAD2;			//包头
//	USART1_TX_DATA[bufid++]=0x02;// length  (93-5+1)  +1 check   //不填写，最后更新

//	USART1_TX_DATA[bufid++]=0;// length  (93-5+1)  +1 check   //不填写，最后更新
//	length_buf_id = bufid-1;
	
	fvalue.fData = sss;//Pitch角度
	USART1_TX_DATA[bufid++]=fvalue.cData[0];
	USART1_TX_DATA[bufid++]=fvalue.cData[1];
	USART1_TX_DATA[bufid++]=fvalue.cData[2];
	USART1_TX_DATA[bufid++]=fvalue.cData[3];

	fvalue.fData = oldsss;//Roll角度
	USART1_TX_DATA[bufid++]=fvalue.cData[0];
	USART1_TX_DATA[bufid++]=fvalue.cData[1];
	USART1_TX_DATA[bufid++]=fvalue.cData[2];
	USART1_TX_DATA[bufid++]=fvalue.cData[3];

	fvalue.fData = iirsss;//Roll轴角速度
	USART1_TX_DATA[bufid++]=fvalue.cData[0];
	USART1_TX_DATA[bufid++]=fvalue.cData[1];
	USART1_TX_DATA[bufid++]=fvalue.cData[2];
	USART1_TX_DATA[bufid++]=fvalue.cData[3];

	fvalue.fData = 0;//Yaw轴角速度
	USART1_TX_DATA[bufid++]=fvalue.cData[0];
	USART1_TX_DATA[bufid++]=fvalue.cData[1];
	USART1_TX_DATA[bufid++]=fvalue.cData[2];
	USART1_TX_DATA[bufid++]=fvalue.cData[3];

//	USART1_TX_DATA[bufid]=get_add_check(USART1_TX_DATA,4,bufid-1);  //加和校验 校验到前一个字节				
//	USART1_TX_DATA[length_buf_id]=(bufid+1)/*总字节*/-4/*HEAD&ID&LENGTH*/-1/*校验位*/;  //已包括校验位////更新长度字节

	Uart1Sends(bufid+1);  //发送

	return bufid;
}

