#ifndef __ahrs_H
#define __ahrs_H

extern float imupitch,imuroll,imuyaw,ImuKp;

void IMUupdate(float gx, float gy, float gz, float ax, float ay, float az, float *pitch, float *roll, float *yaw,float sampleFreq);
#endif
