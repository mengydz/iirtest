#include "imu.h"			
#include "math.h"

//#define RtA 	57.324841f				//弧度到角度
//#define AtR    	0.0174533f				//弧度到角度
//#define Acc_G 	0.0011963f				//加速度变成G
//#define Gyro_G 	0.0610351f				//角速度变成度   此参数对应陀螺2000度每秒
//#define Gyro_Gr	0.0010653f				//角速度变成弧度	此参数对应陀螺2000度每秒

///////////////////////////////////////////////////////////////////////////////
float ImuKp=10.0f;                      	// 加速度权重，越大则向加速度测量值收敛越快
#define Ki 0.0f                		//误差积分增益
//#define sampleFreq	400.0f			// sample frequency in Hz

float imupitch,imuroll,imuyaw;

float q0 = 1.0, q1 = 0.0, q2 = 0.0, q3 = 0.0;    // quaternion elements representing the estimated orientation
float exInt = 0, eyInt = 0, ezInt = 0;    // scaled integral error
void IMUupdate(float gx, float gy, float gz, float ax, float ay, float az, float *pitch, float *roll, float *yaw,float sampleFreq)
{
	float norm;
	//  float hx, hy, hz, bx, bz;
	float vx, vy, vz;// wx, wy, wz;
	float ex, ey, ez;

	// ???????????
	float q0q0 = q0*q0;
	float q0q1 = q0*q1;
	float q0q2 = q0*q2;
//	float q0q3 = q0*q3;
	float q1q1 = q1*q1;
//	float q1q2 = q1*q2;
	float q1q3 = q1*q3;
	float q2q2 = q2*q2;
	float q2q3 = q2*q3;
	float q3q3 = q3*q3;

	if(ax*ay*az==0)
	return;


	norm = sqrt(ax*ax + ay*ay + az*az);       //acc?????
	ax = ax /norm;
	ay = ay / norm;
	az = az / norm;

	// estimated direction of gravity and flux (v and w)              ?????????/??
	vx = 2*(q1q3 - q0q2);												//????xyz???
	vy = 2*(q0q1 + q2q3);
	vz = q0q0 - q1q1 - q2q2 + q3q3 ;

	// error is sum of cross product between reference direction of fields and direction measured by sensors
	ex = (ay*vz - az*vy) ;                           					 //???????????????
	ey = (az*vx - ax*vz) ;
	ez = (ax*vy - ay*vx) ;

	exInt = exInt + ex * Ki;								  //???????
	eyInt = eyInt + ey * Ki;
	ezInt = ezInt + ez * Ki;

	// adjusted gyroscope measurements
	gx = gx + ImuKp*ex + exInt;					   							//???PI???????,???????
	gy = gy + ImuKp*ey + eyInt;
	gz = gz + ImuKp*ez + ezInt;				   							//???gz????????????????,??????????????

	// integrate quaternion rate and normalise						   //????????
	q0 = q0 + (-q1*gx - q2*gy - q3*gz)*(0.5f * sampleFreq);
	q1 = q1 + (q0*gx + q2*gz - q3*gy)*(0.5f * sampleFreq);
	q2 = q2 + (q0*gy - q1*gz + q3*gx)*(0.5f * sampleFreq);
	q3 = q3 + (q0*gz + q1*gy - q2*gx)*(0.5f * sampleFreq);

	// normalise quaternion
	norm = sqrt(q0*q0 + q1*q1 + q2*q2 + q3*q3);

	q0 = q0 / norm;
	q1 = q1 / norm;
	q2 = q2 / norm;
	q3 = q3 / norm;

	*yaw = -atan2(2 * q1 * q2 + 2 * q0 * q3, -2 * q2*q2 - 2 * q3* q3 + 1)* 57.3; // yaw
	*roll = asin(-2 * q1 * q3 + 2 * q0* q2)* 57.3; //roll
	*pitch = atan2(2 * q2 * q3 + 2 * q0 * q1, -2 * q1 * q1 - 2 * q2* q2 + 1)* 57.3; //pitch
}


/***************************END OF FILE**********************************************************************/
