#include "sys.h"
#include "usart.h"	
#include "UserMath.h"
#include "motor.h"
#include "pid.h"
#include "stmflash.h"
/******************** (C) COPYRIGHT 2016 MiraRobot ***************************
 * 文 件 名：main.c
 * 描    述：包含程序的硬件初始化         
 * 实验平台：飞宇 Gimbal V1.0
 * 硬件连接：------------------------
 *          | PB6 - USART1(Tx)      |
 *          | PB7 - USART1(Rx)      |
 *           ------------------------
 * 库 版 本：ST3.5.0
 * 作    者：my.chen@mirarobot.com
 * 论    坛：
**********************************************************************************/
u8 USART1_TX_DATA[USART1_TX_LEN]; 
u8 USART1_RX_DATA[USART1_RX_LEN]; 
u8 USART1_TX_Finish=1;// USART1发送完成标志量
u8 USART3_TX_DATA[USART3_TX_LEN]; 
u8 USART3_RX_DATA[USART3_RX_LEN]; 
u8 USART3_TX_Finish=1;// USART1发送完成标志量
uint8_t needcalibration = 0;
uint16_t TargetAngle=0;
/*
 * 函数名：UART1_DMA_Configuration
 * 描述  ：DMA工作模式配置.
 * 输入  ：无
 * 输出  : 无
 * 调用  ：外部调用
 */
void UART1_DMA_Configuration(void)
{
	DMA_InitTypeDef DMA_InitStructure;
	/* DMA clock enable */
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);//DMA1
	/* DMA1 Channel4 (triggered by USART1 Tx event) Config */
	DMA_DeInit(DMA1_Channel4); 
	DMA_InitStructure.DMA_PeripheralBaseAddr = (u32)&USART1->DR;
	DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)USART1_TX_DATA;
	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralDST;
	DMA_InitStructure.DMA_BufferSize = USART1_TX_LEN;
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
	DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
	DMA_InitStructure.DMA_Priority = DMA_Priority_Low;
	DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
	DMA_Init(DMA1_Channel4, &DMA_InitStructure);
	DMA_ITConfig(DMA1_Channel4, DMA_IT_TC, ENABLE);
	DMA_ITConfig(DMA1_Channel4, DMA_IT_TE, ENABLE);
	/* Enable USART1 DMA TX request */
	USART_DMACmd(USART1, USART_DMAReq_Tx, ENABLE);
	DMA_Cmd(DMA1_Channel4, DISABLE);
	/* DMA1 Channel5 (triggered by USART1 Rx event) Config */
	DMA_DeInit(DMA1_Channel5); 
	DMA_InitStructure.DMA_PeripheralBaseAddr = (u32)&USART1->DR;
	DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)USART1_RX_DATA;
	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;
	DMA_InitStructure.DMA_BufferSize = USART1_RX_LEN;
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
	DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
	DMA_InitStructure.DMA_Priority = DMA_Priority_VeryHigh;
	DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
	DMA_Init(DMA1_Channel5, &DMA_InitStructure);
	DMA_ITConfig(DMA1_Channel5, DMA_IT_TC, ENABLE);
	DMA_ITConfig(DMA1_Channel5, DMA_IT_TE, ENABLE);
	
	/* Enable USART1 DMA RX request */
	USART_DMACmd(USART1, USART_DMAReq_Rx, ENABLE);
	DMA_Cmd(DMA1_Channel5, ENABLE);
}
  
/*
 * 函数名：UART3_DMA_Configuration
 * 描述  ：DMA工作模式配置.
 * 输入  ：无
 * 输出  : 无
 * 调用  ：外部调用
 */
void UART3_DMA_Configuration(void)
{
	DMA_InitTypeDef DMA_InitStructure;
	/* DMA clock enable */
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);//DMA1
	/* DMA1 Channel2 (triggered by USART1 Tx event) Config */
	DMA_DeInit(DMA1_Channel2); 
	DMA_InitStructure.DMA_PeripheralBaseAddr = (u32)(&USART3->DR);
	DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)USART3_TX_DATA;
	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralDST;
	DMA_InitStructure.DMA_BufferSize = USART3_TX_LEN;
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
	DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
	DMA_InitStructure.DMA_Priority = DMA_Priority_Low;
	DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
	DMA_Init(DMA1_Channel2, &DMA_InitStructure);
	DMA_ITConfig(DMA1_Channel2, DMA_IT_TC, ENABLE);
	DMA_ITConfig(DMA1_Channel2, DMA_IT_TE, ENABLE);
	/* Enable USART1 DMA TX request */
	USART_DMACmd(USART3, USART_DMAReq_Tx, ENABLE);
	DMA_Cmd(DMA1_Channel2, DISABLE);
	/* DMA1 Channel3 (triggered by USART3 Rx event) Config */
	DMA_DeInit(DMA1_Channel3); 
	DMA_InitStructure.DMA_PeripheralBaseAddr = (u32)(&USART3->DR);
	DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)USART3_RX_DATA;
	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;
	DMA_InitStructure.DMA_BufferSize = USART3_RX_LEN;
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
	DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
	DMA_InitStructure.DMA_Priority = DMA_Priority_VeryHigh;
	DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
	DMA_Init(DMA1_Channel3, &DMA_InitStructure);
	DMA_ITConfig(DMA1_Channel3, DMA_IT_TC, ENABLE);
	DMA_ITConfig(DMA1_Channel3, DMA_IT_TE, ENABLE);
	
	/* Enable USART1 DMA RX request */
	USART_DMACmd(USART3, USART_DMAReq_Rx, ENABLE);
	DMA_Cmd(DMA1_Channel3, ENABLE);
}
  
void uart1_init(u32 bound)
{
    //GPIO端口设置
    GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	 
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_USART1,ENABLE);//打开重映射时钟和USART重映射后的I/O口引脚时钟
    //USART1_TX   PA9
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9; //PA9
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;	//复用推挽输出
    GPIO_Init(GPIOA, &GPIO_InitStructure);
   
    //USART1_RX	  PA10
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;//浮空输入
    GPIO_Init(GPIOA, &GPIO_InitStructure);  

	UART1_DMA_Configuration();//DMA配置
	
   //Usart1 NVIC 配置
    NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=1 ;//抢占优先级1
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;		//子优先级1
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;			//IRQ通道使能
	NVIC_Init(&NVIC_InitStructure);	//根据指定的参数初始化VIC寄存器
	/*Enable DMA Channel4 Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = DMA1_Channel4_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
  
   //USART 初始化设置

	USART_InitStructure.USART_BaudRate = bound;//一般设置为9600;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;//字长为8位数据格式
	USART_InitStructure.USART_StopBits = USART_StopBits_1;//一个停止位
	USART_InitStructure.USART_Parity = USART_Parity_No;//无奇偶校验位
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;//无硬件数据流控制
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;	//收发模式
    USART_Init(USART1, &USART_InitStructure); //初始化串口

    USART_ITConfig(USART1, USART_IT_IDLE , ENABLE);//空闲中断
    USART_Cmd(USART1, ENABLE);                    //使能串口 
	/* CPU的小缺陷：串口配置好，如果直接Send，则第1个字节发送不出去
	如下语句解决第1个字节无法正确发送出去的问题 */
	USART_ClearFlag(USART1, USART_FLAG_TC); /* 清发送外城标志，Transmission Complete flag */
}

void uart3_init(u32 bound)
{
    //GPIO端口设置
    GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	 
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB,ENABLE);//打开重映射时钟和USART重映射后的I/O口引脚时钟
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3,ENABLE);//打开重映射时钟和USART重映射后的I/O口引脚时钟
    //USART3_TX   PB10
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10; //PB10
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;	//复用推挽输出
    GPIO_Init(GPIOB, &GPIO_InitStructure);
   
    //USART3_RX	  PB11
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;//浮空输入
    GPIO_Init(GPIOB, &GPIO_InitStructure);  

	UART3_DMA_Configuration();//DMA配置
	
   //Usart1 NVIC 配置
    NVIC_InitStructure.NVIC_IRQChannel = USART3_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=2 ;//抢占优先级1
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;		//子优先级1
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;			//IRQ通道使能
	NVIC_Init(&NVIC_InitStructure);	//根据指定的参数初始化VIC寄存器
	/*Enable DMA Channel2 Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = DMA1_Channel2_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
  
   //USART 初始化设置

	USART_InitStructure.USART_BaudRate = bound;//一般设置为9600;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;//字长为8位数据格式
	USART_InitStructure.USART_StopBits = USART_StopBits_1;//一个停止位
	USART_InitStructure.USART_Parity = USART_Parity_No;//无奇偶校验位
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;//无硬件数据流控制
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;	//收发模式
    USART_Init(USART3, &USART_InitStructure); //初始化串口

    USART_ITConfig(USART3, USART_IT_IDLE , ENABLE);//空闲中断
    USART_Cmd(USART3, ENABLE);                    //使能串口 
	/* CPU的小缺陷：串口配置好，如果直接Send，则第1个字节发送不出去
	如下语句解决第1个字节无法正确发送出去的问题 */
	USART_ClearFlag(USART3, USART_FLAG_TC); /* 清发送外城标志，Transmission Complete flag */
}

/*******************************************************************************
* 函数名: 串口DMA发送函数
* 功  能: Uart1Sends函数
* 输  入: char* data_at 字符串头地址  uchar data_len 字符串长度
* 输  出: None
* 返回值: None
*******************************************************************************/
void Uart1Sends(u8 data_len)		   //DMA发送
{
	while(!USART1_TX_Finish);
	//USART用DMA传输替代查询方式发送，克服被高优先级中断而产生丢帧现象。
	DMA_Cmd(DMA1_Channel4, DISABLE); //改变datasize前先要禁止通道工作
	DMA1_Channel4->CNDTR=data_len;
	USART1_TX_Finish=0;
	DMA_Cmd(DMA1_Channel4, ENABLE); 
}

/*******************************************************************************
* 函数名: 串口DMA发送函数
* 功  能: Uart3Sends函数
* 输  入: char* data_at 字符串头地址  uchar data_len 字符串长度
* 输  出: None
* 返回值: None
*******************************************************************************/
void Uart3Sends(u8 data_len)			//DMA发送
{
	while(!USART3_TX_Finish);
	//USART用DMA传输替代查询方式发送，克服被高优先级中断而产生丢帧现象。
	DMA_Cmd(DMA1_Channel2, DISABLE); //改变datasize前先要禁止通道工作
	DMA1_Channel2->CNDTR=data_len;
	USART3_TX_Finish=0;
	DMA_Cmd(DMA1_Channel2, ENABLE); 
}

void USART1_IRQHandler(void)				//串口1中断服务程序
{
	u16 DATA_LEN;
	u16 DATACLR;
	if(USART_GetITStatus(USART1, USART_IT_IDLE) != RESET)//如果为空闲总线中断
	{
		DMA_Cmd(DMA1_Channel5, DISABLE);//关闭DMA,防止处理其间有数据
		DATA_LEN=512-DMA_GetCurrDataCounter(DMA1_Channel5); 
		if(DATA_LEN > 0)//&&get_add_check(USART1_RX_DATA,4,USART1_RX_DATA[3]+4) == USART1_RX_DATA[USART1_RX_DATA[3]+4])
		{

		}
		DMA_ClearFlag(DMA1_FLAG_GL5 | DMA1_FLAG_TC5 | DMA1_FLAG_TE5 | DMA1_FLAG_HT5);//清标志
		DMA1_Channel5->CNDTR = 512;//重装填
		DMA_Cmd(DMA1_Channel5, ENABLE);//处理完,重开DMA
		//读SR后读DR清除Idle
		DATACLR = USART1->SR;
		DATACLR = USART1->DR;
	}
	if(USART_GetITStatus(USART1, USART_IT_PE | USART_IT_FE | USART_IT_NE) != RESET)//出错
	{
		USART_ClearITPendingBit(USART1, USART_IT_PE | USART_IT_FE | USART_IT_NE);
	}
	USART_ClearITPendingBit(USART1, USART_IT_TC);
	USART_ClearITPendingBit(USART1, USART_IT_IDLE);
} 

void USART3_IRQHandler(void)                	//串口1中断服务程序
{
	u16 DATA_LEN;
	u16 DATACLR;
	if(USART_GetITStatus(USART3, USART_IT_IDLE) != RESET)//如果为空闲总线中断
	{
		DMA_Cmd(DMA1_Channel3, DISABLE);//关闭DMA,防止处理其间有数据
		DATA_LEN=512-DMA_GetCurrDataCounter(DMA1_Channel3); 
		if(DATA_LEN > 0)
		{ 
			
		}
		DMA_ClearFlag(DMA1_FLAG_GL3 | DMA1_FLAG_TC3 | DMA1_FLAG_TE3 | DMA1_FLAG_HT3);//清标志
		DMA1_Channel3->CNDTR = 512;//重装填
		DMA_Cmd(DMA1_Channel3, ENABLE);//处理完,重开DMA
		//读SR后读DR清除Idle
		DATACLR = USART3->SR;
		DATACLR = USART3->DR;
	}
	if(USART_GetITStatus(USART3, USART_IT_PE | USART_IT_FE | USART_IT_NE) != RESET)//出错
	{
		USART_ClearITPendingBit(USART3, USART_IT_PE | USART_IT_FE | USART_IT_NE);
	}
	USART_ClearITPendingBit(USART3, USART_IT_TC);
	USART_ClearITPendingBit(USART3, USART_IT_IDLE);
} 

//DMA1_Channel4中断服务函数
//USART1使用DMA发数据中断服务程序
void DMA1_Channel4_IRQHandler(void)
{
	DMA_Cmd(DMA1_Channel4, DISABLE);//关闭DMA
	DMA_ClearITPendingBit(DMA1_IT_TC4);
	DMA_ClearITPendingBit(DMA1_IT_TE4);
	USART1_TX_Finish=1;//置DMA传输完成
}

//DMA1_Channel2中断服务函数
//USART3使用DMA发数据中断服务程序
void DMA1_Channel2_IRQHandler(void)
{
	DMA_Cmd(DMA1_Channel2, DISABLE);//关闭DMA
	DMA_ClearITPendingBit(DMA1_IT_TC2);
	DMA_ClearITPendingBit(DMA1_IT_TE2);
	USART3_TX_Finish=1;//置DMA传输完成
}

