#ifndef __USART_H
#define __USART_H
#include "stdio.h"	
#include "sys.h" 


#define USART1_RX_LEN  			512  	//定义最大接收字节数 100
#define USART1_TX_LEN  			512  	//定义最大发送字节数 100
#define USART3_RX_LEN  			512  	//定义最大接收字节数 100
#define USART3_TX_LEN  			512  	//定义最大发送字节数 100
	  	
extern u8 USART1_TX_DATA[USART1_TX_LEN]; //发送缓冲,最大USART_REC_LEN个字节.末字节为换行符 
extern u8 USART1_RX_DATA[USART1_RX_LEN]; //接收缓冲,最大USART_REC_LEN个字节.末字节为换行符 

extern u8 USART3_TX_DATA[USART3_TX_LEN]; //发送缓冲,最大USART_REC_LEN个字节.末字节为换行符 
extern u8 USART3_RX_DATA[USART3_RX_LEN]; //接收缓冲,最大USART_REC_LEN个字节.末字节为换行符 

//如果想串口中断接收，请不要注释以下宏定义
void uart1_init(u32 bound);
void Uart1Sends(u8 data_len);		   //DMA发送
//如果想串口中断接收，请不要注释以下宏定义
void uart3_init(u32 bound);
void Uart3Sends(u8 data_len);		   //DMA发送

extern uint8_t needcalibration;
extern uint16_t TargetAngle;
#endif


