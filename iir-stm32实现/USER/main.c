#include "led.h"
#include "delay.h"
#include "sys.h"
#include "usart.h"
#include "mpu6050.h"
#include "stmflash.h"
#include "ANO_TC_STM32F1_I2C.h"
#include "motor.h"
#include "timer.h"
#include "uprotocol.h"
#include "pid.h"
#include "24cxx.h"
#include "UserMath.h"
#include "imu.h"
#include "iir.h"
#include "math.h"

float sss,iirsss,oldsss;

/******************** (C) COPYRIGHT 2016 MiraRobot *****************************
 * 文 件 名：main.c
 * 描    述：包含程序的硬件初始化         
 * 实验平台：定制4K相机
 * 硬件连接： -----------------------
 *          | PB6 - USART1(Tx)      |
 *          | PB7 - USART1(Rx)      |
 *           -----------------------
 * 库 版 本：ST3.5.0
 * 作    者：my.chen@mirarobot.com
 * 网    址：www.mirarobot.com 
**********************************************************************************/

void Main_loop_10MS_First(void);
void Main_loop_20MS_First(void);
void Main_loop_100MS_First(void);
void Main_loop_200MS_First(void);
u8 loop_idx10ms=0;
u8 loop_idx20ms=0;
u8 loop_idx100ms=0;
u8 loop_idx200ms=0;

//DESIGN_SPECIFICATION IIR_Filter;

int main(void)
{	
//	static uint64_t time_pre_us = 0;
	
	delay_init();			//延时函数初始化
	NVIC_Configuration(); 	//设置NVIC中断分组2:2位抢占优先级，2位响应优先级
	LED_Init();
	delay_ms(100);
	uart1_init(115200);
	TIM4_Int_Init(50000,72-1);
	TIM3_Int_Init(50000,72-1);

//	IIR_Filter.Cotoff = COFOFF_BAND;       							//[red/sec]
//	IIR_Filter.Stopband = STOP_BAND;   									//[red/sec]
//	IIR_Filter.Stopband_attenuation = STOP_ATTENUATION;	//[dB]
	
	double az[M+1] , bz[M+1];
	Direct(COFOFF_BAND,STOP_BAND,STOP_ATTENUATION,M,az,bz);
//	Direct(IIR_Filter.Cotoff,IIR_Filter.Stopband,IIR_Filter.Stopband_attenuation,M,az,bz);

	double *Memory_Buffer;
	Memory_Buffer = (double *) malloc(sizeof(double)*(M+1));  
	memset(Memory_Buffer,0,sizeof(double)*(M+1));
	while(1)
	{
		while(Wait_processing(1000));

		loop_idx10ms++;
		if(loop_idx10ms>=10) 
		{
			loop_idx10ms=0;
			static uint32_t tt=0;
			float s8 	= sin(16 * PI * tt/1000); 
			float s20	= sin(40 * PI * tt / 1000);
			float s100	= sin(200 * PI * tt / 1000);
			float s300	= sin(600 * PI * tt / 1000);
			sss = s8 + s20 + s100;// + s300;
			oldsss = 0.01*sss+0.99*oldsss;
			iirsss = IIRFilter(  az, (M+1),bz, (M+1),sss,Memory_Buffer );
			Upload_Gcs_data();
			tt++;
			Main_loop_10MS_First();
		}//////////////////////////////////////////10HZ	

		loop_idx20ms++;
		if(loop_idx20ms>=20) 
		{
			loop_idx20ms=0;
			Main_loop_20MS_First();
		}//////////////////////////////////////////10HZ	

		loop_idx100ms++;
		if(loop_idx100ms>=100) 
		{
			loop_idx100ms=0;
			Main_loop_100MS_First();  
		}//////////////////////////////////////////100HZ	

		loop_idx200ms++;
		if(loop_idx200ms>=100) 
		{
			loop_idx200ms=0;
			Main_loop_200MS_First();
		}//////////////////////////////////////////100HZ	
	}
}

void Main_loop_10MS_First()
{
}

void Main_loop_20MS_First()
{
}

void Main_loop_100MS_First()
{
	LED0=!LED0;
}

void Main_loop_200MS_First()
{

}

